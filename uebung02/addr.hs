type Nibble = (Bool, Bool, Bool, Bool)

--Hilfsfunktionen für Aufgabe 1.1
dualToDec :: Nibble -> Int
dualToDec (a, b, c, d) = (if a then 8 else 0) + (if b then 4 else 0) + (if c then 2 else 0) + (if d then 1 else 0)

twosCompl :: Nibble -> Int
twosCompl (a, b, c, d) = (if a then -8 else 0) + (if b then 4 else 0) + (if c then 2 else 0) + (if d then 1 else 0)

nibbleBits :: Nibble -> String 
nibbleBits (a, b, c, d) = (if a then "1" else "0") ++ (if b then "1" else "0") ++ (if c then "1" else "0") ++ (if d then "1" else "0")

--Aufg 1.1
showNibble :: Nibble -> String
showNibble n = nibbleBits (n) ++ " " ++ show (dualToDec (n)) ++ " " ++ show (twosCompl (n))

--Hilfsfunktionen für Aufgabe 1.2
xor :: (Bool, Bool) -> Bool
xor (a, b) = (a || b) && (not (a && b))

--Aufg 1.2
fulladder :: Bool -> Bool -> Bool -> (Bool, Bool)
fulladder a b cin = (s, cout)
    where 
        t = xor (a,b)
        s = xor (t,cin)
        d0 = a && b 
        d1 = t && cin
        cout = d0 || d1

--Hilfsfunktionen für Aufgabe 1.3


--Aufg 1.3
--per Skript hat ein RCA 2n Dateneingänge und n+1 Datenausgänge, aber Nibble als Ausgabe hat nur 4 Bits
--Deshalb wird der Übertrag C verworfen
rippleCarryAdder :: Nibble -> Nibble -> Nibble
rippleCarryAdder (a3, a2, a1, a0) (b3, b2, b1, b0) = (s3,s2,s1,s0)
    where 
        (s0,c0) = fulladder a0 b0 False
        (s1,c1) = fulladder a1 b1 c0
        (s2,c2) = fulladder a2 b2 c1
        (s3,c3) = fulladder a3 b3 c2

--Aufg 1.4
tableAdder :: (Nibble -> Nibble -> Nibble) -> [(Nibble, Nibble)] -> String
tableAdder f [(a,b)] = showNibble a ++ " ; " ++ showNibble b ++ " = " ++ showNibble (f a b)