#Informatik II, Übung 02, Aufgabe 2

Markdown und AsciiMath

##Addierer mit Übertragsauswahl 

###Aufgabe 1
  
Bei einem carry select adder beträgt die Tiefe für ein $2^k$-Bit Addierer $k/2 + 2$, wobei $k in NN, k >= 0, 2$ sich aus einem MUX und einem full adder zum Übertrag ergibt.
 
###Aufgabe 2
 
Überlegung: zunächst die niedrigste Anzahl von Wörtern finden, die mit einem carry select adder (im Folgenden CSA abgekürzt), addiert werden können.
Bei der Addition von einzelnen Bits kann man nur ein adder-Gatter verwenden, wodurch dieser Fall nicht betrachtet wird.
Das wären Wörter mit Länge $2^1$-Bits, also zwei Stellen. Dafür wird ein adder-Gatter für die eig. Addition, ein adder-Gatter für den Übertrag und ein MUX-Gatter verwendet (bspw. bei "01" + "01"). 
Somit wäre die mindest Menge 3 Gatter für ein CSA.
Bei Wörtern mit $2^2$-Bits, würde bei einem worst case wie"0011" + "0001" zwei CSA für die Addition sowie ein adder-Gatter für den Übertrag und ein MUX-Gatter.
Bei $2^3$ würden dann drei CSA sowie ein adder- und ein MUX-Gatter verwendet.
 
Daraus folgt eine Tiefe von $k*3+2$, da ein CSA aus drei Gattern besteht, stets ein adder-Gatter und ein Mux-Gatter im worst-case durchlaufen wird und weitere Additionen durch CSAs realisiert werden.

