type Nibble = (Bool, Bool, Bool, Bool)
type Byte = (Nibble, Nibble)

--Aufg. 1.1
shiftByte :: Byte -> Byte
shiftByte ((a7,a6,a5,a4), (a3,a2,a1,a0)) = ((a6,a5,a4,a3), (a2,a1,a0, False))

--Aufg. 1.2
signExtension :: Nibble -> Byte
signExtension (a3,a2,a1,a0) = ((a3,a3,a3,a3), (a3,a2,a1,a0))

--Aufg. 1.3
multBitByte :: Bool -> Byte -> Byte
multBitByte False b = ((False,False,False,False), (False,False,False,False))
multBitByte True b = b

--Aus Aufg 1.2 kopiert

xor :: (Bool, Bool) -> Bool
xor (a, b) = (a || b) && (not (a && b))

fulladder :: Bool -> Bool -> Bool -> (Bool, Bool)
fulladder a b cin = (s, cout)
    where 
        t = xor (a,b)
        s = xor (t,cin)
        d0 = a && b 
        d1 = t && cin
        cout = d0 || d1


--Aufg. 1.4
--s = sum
--d = Übertrag
rippleCarryAdder :: Byte -> Byte -> Byte
rippleCarryAdder ((a7,a6,a5,a4),(a3,a2,a1,a0)) ((b7,b6,b5,b4),(b3,b2,b1,b0)) = ((s7,s6,s5,s4),(s3,s2,s1,s0))
    where 
        (s0,d0) = fulladder a0 b0 False
        (s1,d1) = fulladder a1 b1 d0
        (s2,d2) = fulladder a2 b2 d1
        (s3,d3) = fulladder a3 b3 d2
        (s4,d4) = fulladder a4 b4 d3
        (s5,d5) = fulladder a5 b5 d4
        (s6,d6) = fulladder a6 b6 d5
        (s7,d7) = fulladder a7 b7 d6

--Aufg. 1.5
--s = sum
--d = Übertrag
carrySaveAdder :: Byte -> Byte -> Byte -> (Byte, Byte)
carrySaveAdder ((a7,a6,a5,a4),(a3,a2,a1,a0)) ((b7,b6,b5,b4),(b3,b2,b1,b0)) ((c7,c6,c5,c4),(c3,c2,c1,c0)) = (((s7,s6,s5,s4),(s3,s2,s1,s0)),((d7,d6,d5,d4),(d3,d2,d1,d0)))
    where
        (u0,v0) = fulladder a0 b0 c0
        (u1,v1) = fulladder a1 b1 c1
        (u2,v2) = fulladder a2 b2 c2
        (u3,v3) = fulladder a3 b3 c3
        (u4,v4) = fulladder a4 b4 c4
        (u5,v5) = fulladder a5 b5 c5
        (u6,v6) = fulladder a6 b6 c6
        (u7,v7) = fulladder a7 b7 c7
        --Byte shift durchführen, um den Übertrag zu verdoppeln
        ((x7,x6,x5,x4),(x3,x2,x1,x0)) = shiftByte ((v7,v6,v5,v4),(v3,v2,v1,v0))
        --neuer Übertrag auf Summe addieren
        --fulladder mit Zwischensumme, 2*Übertrag und Übertrag aus voriger Bit-Addition verrechnen 
        (s0,d0) = fulladder u0 v0 False
        (s1,d1) = fulladder u1 x1 d0
        (s2,d2) = fulladder u2 x2 d1
        (s3,d3) = fulladder u3 x3 d2
        (s4,d4) = fulladder u4 x4 d3
        (s5,d5) = fulladder u5 x5 d4
        (s6,d6) = fulladder u6 x6 d5
        (s7,d7) = fulladder u7 x7 d6

--Aufg. 1.6
--Alle bits invertieren und dann +1 addieren
negByte :: Byte -> Byte
negByte ((a7,a6,a5,a4),(a3,a2,a1,a0)) = ((z7,z6,z5,z4),(z3,z2,z1,z0))
    where 
        (z0,c0) = fulladder (not a0) True False
        (z1,c1) = fulladder (not a1) c0 False
        (z2,c2) = fulladder (not a2) c1 False
        (z3,c3) = fulladder (not a3) c2 False
        (z4,c4) = fulladder (not a4) c3 False
        (z5,c5) = fulladder (not a5) c4 False
        (z6,c6) = fulladder (not a6) c5 False
        (z7,c7) = fulladder (not a7) c6 False

--Aufg. 1.7
multNibble :: Nibble -> Nibble -> Byte
multNibble a (b3,b2,b1,b0) = z
    where
        --Byte von Nibble a bilden 
        c = signExtension a
        --b bitweise mit c multiplizieren
        --kein Byte von b gebildet, da es nahezu identisch wäre
        --shiftByte für korrektes Stellen verschieben wie bei schriftlicher Multiplikation
        s0 = multBitByte b0 c
        s1 = shiftByte (multBitByte b1 c) 
        s2 = shiftByte (shiftByte (multBitByte b2 c)) 
        s3 = shiftByte (shiftByte (shiftByte (multBitByte b3 c))) 
        s4 = shiftByte (shiftByte (shiftByte (shiftByte (multBitByte b3 c)))) 
        s5 = shiftByte (shiftByte (shiftByte (shiftByte (shiftByte (multBitByte b3 c))))) 
        s6 = shiftByte (shiftByte (shiftByte (shiftByte (shiftByte (shiftByte (multBitByte b3 c)))))) 
        s7 = shiftByte (shiftByte (shiftByte (shiftByte (shiftByte (shiftByte (shiftByte (multBitByte b3 c))))))) 
        --summen bilden von den Teilergebnissen inkl. Überträge und Zwischensummen aus Multiplikation
        (u0,v0) = carrySaveAdder s0 s1 s2
        x0 = shiftByte v0
        (u1,v1) = carrySaveAdder s3 x0 u0
        x1 = shiftByte (shiftByte v1)
        (u2,v2) = carrySaveAdder s4 x1 u1
        x2 = shiftByte (shiftByte (shiftByte (v2)))
        (u3,v3) = carrySaveAdder s5 u2 x2
        x3 = shiftByte (shiftByte (shiftByte (shiftByte (v3))))
        (u4,v4) = carrySaveAdder s6 u3 x3
        x4 = shiftByte (shiftByte (shiftByte (shiftByte (shiftByte (v4)))))
        (u5,v5) = carrySaveAdder s7 u4 x4
        x5 = shiftByte (shiftByte (shiftByte (shiftByte (shiftByte (shiftByte (v5))))))
        --Letzten Übertrag mit letzer Zwischensumme addieren
        z = rippleCarryAdder u5 x5

--Hilfsfunktionen für Aufg. 1.8

--fromEnum bildet Bool auf Int ab, False -> 0, True -> 1
showNibble :: Nibble -> String
showNibble n = toBinaryN n ++ "  " ++ show(toDecimalN n) ++ "  " ++ show(to2complementN n)

toBinaryN :: Nibble -> String
toBinaryN (a, b, c, d) = show(if a then 1 else 0) ++ show(if b == True then 1 else 0) ++ show(if c == True then 1 else 0) ++ show(if d == True then 1 else 0)

toDecimalN :: Nibble -> Int
toDecimalN (a, b, c, d) = (if a then 8 else 0) + (if b then 4 else 0) + (if c then 2 else 0) + (if d then 1 else 0)

to2complementN :: Nibble -> Int
to2complementN (a, b, c, d) = if toDecimalN (a, b, c, d) <= 7 then toDecimalN (a, b, c, d) else toDecimalN (a, b, c, d) - 16 

--verbessert zu Übung02.Aufg.2

showByte :: Byte -> String
showByte n = toBinaryB n ++ "  " ++ show(toDecimalB n) ++ "  " ++ show(to2complementB n)

toBinaryB :: Byte -> String
toBinaryB ((a, b, c, d),(e,f,g,h)) = show(fromEnum a) ++ show(fromEnum b) ++ show(fromEnum c) ++ show(fromEnum d) ++ show(fromEnum e) ++ show(fromEnum f) ++ show(fromEnum g) ++ show(fromEnum h)

toDecimalB :: Byte -> Int
toDecimalB ((a, b, c, d),(e,f,g,h)) = fromEnum a * 2^7 + fromEnum b * 2^6 + fromEnum c * 2^5 + fromEnum d * 2^4 + fromEnum e * 2^3 + fromEnum f * 2^2 + fromEnum g * 2^1 + fromEnum h * 2^0

to2complementB :: Byte -> Int
to2complementB b = if toDecimalB b <= (2^7 - 1) then toDecimalB b else (toDecimalB b) - 2^8 

--Aufg. 1.8
tableMult :: (Nibble -> Nibble -> Byte) -> [(Nibble, Nibble)] -> String
tableMult f [] = ""
tableMult f l = showNibble (n0) ++ " * " ++ showNibble (n1) ++ " = " ++ showByte (f n0 n1) ++ "\n" ++ tableMult f (tail l)
    where
        n0 = fst (head l)
        n1 = snd (head l)