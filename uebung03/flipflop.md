#Informatik II, Übung 03, Aufgabe 2

Markdown und AsciiMath

##Anwendung von Flip-Flops

###Aufgabe 1

|      | C | A | B | 2B+A |
|------|---|---|---|------|
| $z_0$ | 0 | 0 | 0 | 0    |
| $z_0 + t$  | 1 | 1 | 0 | 1    |
| $z_0 + 2t$ | 0 | 1 | 0 | 1    |
| $z_0 + 3t$ | 1 | 0 | 1 | 1    |
| $z_0 + 4t$ | 0 | 0 | 1 | 1    |
| $z_0 + 5t$ | 1 | 1 | 1 | 1    |
| $z_0 + 6t$ | 0 | 1 | 1 | 1    |
| $z_0 + 7t$ | 1 | 0 | 0 | 0    |
| $z_0 + 8t$ | 0 | 0 | 0 | 0    |
| $z_0 + 9t$ | 1 | 1 | 0 | 0    |

Die Periodizität geht von $z_0$ bis $z_0 + 7t$.

###Aufgabe 2

Die Schaltung kann als zweistelliger Binärer Counter verwendet werden. 
Man könnte es auch als binären Addierer verwenden, wobei mit jeder steigenden Flanke von C binär 1 auf addiert wird, $B$ die $2^1$-Stelle und $A$ die $2^0$-Stelle darstellen.