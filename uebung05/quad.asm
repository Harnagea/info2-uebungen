.data
newline: .asciiz "\n"

.text
main:
li $v0, 5
syscall

move $t0, $v0       #eingabe von n

#Initialsieren der Register
li $t1, 0           #ergebnis
li $t2, 0           #laufvariable/counter
li $t3, 1           #ungerade zahl zum aufaddieren

#Prüfen der Eingabezahl
bltz $t0, positiv   #negatives n positiv machen

#Berechnung einer Quadratzahl: Summe der ersten n ungeraden Zahl
loop:
beq $t2, $t0, finish    #abbruch condition counter $t2 == n $t0 
add $t1, $t1, $t3       #ergebnis $1 = ergebnis $t1 + ungerade Zahl $t3
addi $t2, $t2, 1        #counter erhöhen $2 += 1
addi $t3, $t3, 2        #nächste ungerade Zahl bilden $t3 += 2
j loop                  

#Negativ Zahl positiv machen

positiv:
sub $t0, $zero, $t0     #0 - negatives n = positives n
#li $v0, 1
#move $a0, $t0
#syscall
j loop

#Ausgabe der Quadratzahl
finish:
li $v0, 1
move $a0, $t1
syscall
li $v0, 4
la $a0, newline
syscall

#Beenden
li $v0, 10
syscall