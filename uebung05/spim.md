#Informatik II, Übung 05, Aufgabe 1

Markdown und AsciiMath

### Speicherbild

*a)*

```
| 4 Byte (5)                | <- len
| 4 Byte (-54)              |
| 4 Byte (120)              |    
| 4 Byte (-35)              |
| 4 Byte (23)               |
| 4 Byte (-54)              | <- sequence 
+ -- - - - - - - - - - - - -+
| Datensegment              |
|                           |
+ -- - - - - - - - - - - - -+ 0 x1000 0000
| Textsegment               |
|                           |
+ -- - - - - - - - - - - - -+ 0 x0040 0000
| reserviert                |
+ -- - - - - - - - - - - - -+ 0 x0000 0000
```

*b)*
 
```
| 4 Byte (-54)              | <- sequence_end
| 4 Byte (-190)             |
| 4 Byte (3)                |
| 4 Byte (-5)               |
| 4 Byte (2)                | <- sequence_start
+ -- - - - - - - - - - - - -+
| Datensegment              |
|                           |
+ -- - - - - - - - - - - - -+ 0 x1000 0000
| Textsegment               |
|                           |
+ -- - - - - - - - - - - - -+ 0 x0040 0000
| reserviert                |
+ -- - - - - - - - - - - - -+ 0 x0000 0000
```

*c)*
 
```
| 1 Byte (0x00)             | 
| 1 Byte (0x10)             | <- n
| 2 Byte (0x0006)           | <- h , Speicheradresse 18 (da 17 Byte belegt)
| 1 Byte (0x00)             |
| 1 Byte (0x35)             |
| 1 Byte (0x30)             |
| 1 Byte (0x78)             |
| 1 Byte (0x30)             | <- s
| 4 Byte (0x00000004)       | <- z, Speicheradresse 8 (da 7 Byte belegt)
| 1 Byte (0x03)             |
| 1 Byte (0x02)             |
| 1 Byte (0x01)             | <- y
| 4 Byte (0x00000000)       | <- x
+ -- - - - - - - - - - - - -+
| Datensegment              |
|                           |
+ -- - - - - - - - - - - - -+ 0 x1000 0000
| Textsegment               |
|                           |
+ -- - - - - - - - - - - - -+ 0 x0040 0000
| reserviert                |
+ -- - - - - - - - - - - - -+ 0 x0000 0000
```


### Codierung

*a)*

```
.data
#Belegung von Speicherzellen mit den angegebenen Werten
y:  .byte   1, 2, 3, 128 
s:  .asciiz "0x05"
n:  .asciiz "\n"

.text
main:  lw   $a0, y  #Laden des Wortes ab Label y in a0
       li   $v0, 1  #Belegung des Registers v0 mit 1 (für Dezimalzahlen)
       syscall      #Ausgabe von a0 als Int interpretiert (da v0 1 ist)
       la   $a0, n  #Laden des ascii-Zeichens n mit Nullzeichen
       li   $v0, 4  #Belegung des Registers v0 mit 4 (für Strings)
       syscall      #Ausgabe von a0 als String interpretiert
       lw   $a0, s  #Laden des Wortes ab Label s in a0
       li   $v0, 1  #Belegung des Registers v0 mit 1 (für Dezimalzahlen)
       syscall      #Ausgabe von a0 als Int interpretiert
       la   $a0, n  #Laden des ascii-zeichens n mit Nullzeichen
       li   $v0, 4  #Belegung des Registers v0 mit 4 (für Strings)
       syscall      #Ausgabe von a0 als String interpretiert
       li   $v0, 10 #Belegung von v0 mit 10 für reguläres Programmende
       syscall      #Programmende
```

*b)*

Ausgabe mit little-endian:

0x80030201 0x00000a00

0x35307830 0x00000a00

Durch Interpretation mit little-endian werden die Wörter in Zeile 8 und Zeile 14 von der niedrigsten zur höchsten Adresse gelesen. Für das label y bedeutet das, dass zuerst das Byte mit 128 (0x80), dann das Byte mit 3 (0x03), dann das mit 2 (0x02) und zuletzt das Byte mit 1 (0x01) gelesen wird, wodurch 0x80030201 entsteht. Im Anschluss folgt das LF-Zeichen \n (0x0a) inkl. dem Nullzeichen am Ende (0x00) und den führenden Nullen 0x0000.
Danach wird der String s als Wort interpretiert, wodurch die einzelnen Zeichen in ihre Hex-Werte umgewandelt werden. Da es little-endian ist, muss man den String "rückwärts" lesen, also "5" (0x35), "0" (0x30), "x" (0x78), "0" (0x30), wodurch sich die Hexadezimalzahl 0x35307830 ergibt. Danach folgt wieder ein LF und das Programm wird beendet.

Ausgabe mit big-endian:

0x01020308 0x000a0000

0x30783035 0x000a0000

Mit big-endian wird die Lese-Reihenfolge der Speicherzellen umgedreht, d.h. dass von der höchsten zur niedrigsten Adresse. Dadurch wird für Label y, in der Reihenfolge, wie die Bytes im .data-Segment angelegt worden, das Wort gelesen, sprich das Byte mit 1 (0x01), das Byte mit 2 (0x02), das Byte mit 3 (0x03) und zuletzt das Byte mit 128 (0x80). Dadurch ensteht die Ausgabe 0x01020380. Für den String n ist das ähnlich, d.h., dass zuerst das Nullzeichen 0x00, dann das LF-Zeichen 0x05 und der Rest mit Nullen aufgefüllt wird.
Indem der String s als Wort ausgegeben wird, wird nach big-endian der String, wie geschrieben, in hex ausgegeben, also: 0 (0x30), x (0x78), 0 (0x30), 5 (0x35). Darauf folgt die Ausgabe von LF, wie bereits beschrieben.

*c)*

Durch den logischen Bitshift um 4 Stellen befindet sich in a0 nun bei little-endian 0x10003020 und bei big-endian 0x00302010 im Register a0. Die Hexzahlen würden dementsprechend ausgegeben werden.
Der Befehl srl füllt führende Stellen, die durch den shift "freigemacht" werden, mit 0 auf.

*d)*

Durch den arithmetischen Bitshift um 4 Stellen befindet sich in a0 nun bei little-endian 0xf8003020 im Register a0. Bei sra werden führende Stellen mit dem höherwertigsten Bit aufgefüllt, in dem Fall 1, wodurch 0xf8 am Anfang steht.
Bei big-endian wäre die Ausgabe 0x00102030, die führenden Stellen werden mit dem höchsten Bit, hierbei 0, aufgefüllt.

### Branch und Jump

*a)*

Der gesamt Adressbereich von .text umfasst $2^28$ Adressen. Somit ist es nicht möglich, mit dem beq-Befehl vom Beginn des .text-Segments (0x0040 0000) zum Ende (0x0fff fffc) zu springen, da dafür eine 28 bit Adresse benötigt wird. Genauso kann man nicht vom Ende des .text-Segments zum Beginn springen, man müsste zwei beq-Befehle durchführen, wobei die sich mittig im Segment befinden müssten.

*b)*

Duch den bitshift werden die 2 niedrigsten Bits mit 0 gefüllt, wodurch nur Vielfache von 4 angesprungen werden können. Es wäre also möglich, den ganzen .text-Segment anzuspringen außer der Adressen $mod 4 !in [0]_4$. Da aber jeder Befehl eine Länge von 32-Bit hat, ist es möglich, jeden Befehl anzuspringen.

*c)*

Man müsste sicherstellen, dass Befehle sich an jeder vierten Speicherstelle im .text-Segment befinden, damit jeder Befehl angesprungen werden kann. Dafür müsste man das Segment, wie das .data-Segment, aligned strukturieren und jeden Befehl an einer Adresse $mod 4 in [0]_4$ ablegen. Die Beschränkung durch den Bitshift würde enntfallen und alle Befehle wären somit addressierbar.

### Assembler lesen

*a)*

```
        add    $t0,  $zero, $zero   # Addition vom Nullwert mit Nullwert und speichern in Register $t0
loop:   beq    $a1,  $zero, finish  # Kopf eine Schleife mit der Bedingung ($a1 != 0), also wenn $a1 == 0 dann spring zu Label finish
        add    $t0,  $t0,   $a0     # auf $t0 wird der Wert von $a0 auf addiert, s.d. $t0=$t0 + $a0
        addi   $a1,  $a1,   -1      # von $a1 wird 1 abgezogen, s.d. $a1 = $a1 - 1
        j      loop                 # sprung zu Label loop
finish: addi   $t0,  $t0,   100     # auf das Register wird 100 aufaddiert, s.d. $t0 = $t0 + 100
        add    $v0,  $t0,   $zero   # in $v0 wird der Wert $t0 + 0, also nur $t0, gespeichert
```

*b)*

Der Code berechnet $(sum_(i=0)^(b) a)+100$ bzw. $a*b + 100$.