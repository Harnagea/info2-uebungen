#Informatik II, Übung 04, Aufgabe 3

Markdown und AsciiMath

***Bemerkung: die Formatierung der Tabellen bzw. Diagramme funktioniert mit md2pf nur in der erzeugten .html-Datei, nicht in der .pdf oder im Online Umwandler ***

##Pipelining

###Aufgabe 1

    lw $t1, 4($a0) -- lade das Wort in Register a0, Offset 4 und speicher es ins Register t1 (Zugriff und Zwischenspeichern der Variable B)

    lw $t2, 8($a0) -- lade das Wort in Register a0, Offset 8 und speicher es ins Register t2 (Zugriff und Zwischenspeichern der Variable C)

    add $t3, $t1, $t2 -- addiere die Wörter aus t1 und t2 und speichere das Ergebnis in t3 (das "+" in der ersten Zeile des Pseudocodes)

    sw $t3, 0($a0) -- nimm das Wort aus t3 und überschreibe das Wort im Register a0, Adresse 0 damit ("=" in der ersten Zeile des Pseudocodes, wenn A das Ergebnis der Addition von B und C zugewiesen wird)

    lw $t4, 16($a0) -- lade das Wort in Register a0, Offset 16 und speicher es ins Register t4 (Zugriff und Zwischenspeichern der Variable E)

    add $t5, $t1, $t4 -- addiere die Wörter aus t1 und t4 und speichere das Ergebnis in t5 (das "+" in der zweiten Zeile des Pseudocodes, t1 befindet sich noch immer im Register und muss nicht neugeladen werden)

    sw $t5, 12($a0) -- nimm das Wort aus t5 und überschreibe das Wort im Register a0, Adresse 12 damit ("=" in der zweiten Zeile des Pseudocodes, wenn D das Ergebnis der Addition von B und E zugewiesen wird)
    
Für das Register a0 ergibt sich somit folgende Aufteilung:

- Offset 0 = $A$
- Offset 4 = $B$
- Offset 8 = $C$
- Offset 12 = $D$
- Offset 16 = $E$

###Aufgabe 2

Für die Ausführung werden 15 Taktzyklen verwendet, nach folgendem Diagramm:

| Zeile | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
|   1   | LW | ID | ``` 4($a0) ``` | EX             | ```$t1``` | | | | | | | | | | | | 
|   2   |    | LW | ID             | ``` 8($a0) ``` | EX        | ```$t2``` | | | | | | | | | | | 
|   3   |    |    | ADD            | ID             | NOP       | NOP     | ```$t1,$t2``` | + | ```$t3``` | | | | | | | | 
|   4   |    |    |                | SW             | ID        | NOP     | NOP           | NOP| NOP | ```$t3``` | EX | ``` 4($a0) ```| | | | | 
|   5   |    |    |                |              | LW        | ID     |  ``` 16($a0) ```| EX| ```$t4``` |  |  | | | | | |
|   6   |    |    |                |              |         | ADD     |  ID | NOP| NOP | ```$t1,$t4```  | +  |```$t5``` | | | | |
|   7   |    |    |                |              |         |      |  SW | ID| NOP | NOP | NOP  |NOP | ```$t5``` | EX | ```12($a0)``` | |

1-7 beziehen sich auf die jeweiligen MIPS-Zeilen. 
 
Die NOPs in Zeile 3 ergeben sich daraus, dass in Zeile 2 der Load-Befehl nach Register ```$t2``` von dem Wort B noch nicht abgeschlossen ist, wodurch die Addition auf pausiert wird, bis das Register verfügbar ist.
In Zeile 4 wird aus dem selben Grund NOPs ausgeführt, da das Ergebnis der Addition, nämlich der Wert A in Register ```$t3``` noch nicht verfügbar ist, wodurch dieser nicht gespeichert werden kann.
Analog dazu ergibt sich die Verzögerung in Zeile 6 und 7.

###Aufgabe 3

Durch Forwarding werden nur noch 13 Taktzyklen nach folgendem Diagramm benötigt:

| Zeile | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
|   1   | LW | ID | ``` 4($a0) ``` | EX             | ```$t1``` | | | | | | | | |
|   2   |    | LW | ID             | ``` 8($a0) ``` | EX        | ```$t2``` | | | | | | | |
|   3   |    |    | ADD            | ID             | NOP       | ```$t1,$t2``` | + | ```$t3``` | | | | | | 
|   4   |    |    |                | SW             | ID        | NOP     | NOP           | ```$t3``` | EX | ``` 4($a0) ```| | | |
|   5   |    |    |                |              | LW        | ID     |  ``` 16($a0) ```| EX| ```$t4``` |  |  | | |
|   6   |    |    |                |              |         | ADD     |  ID | NOP | ```$t1,$t4```  | +  |```$t5``` | | |
|   7   |    |    |                |              |         |      |  SW | ID| NOP | NOP | ```$t5``` | EX | ```12($a0)```|

1-7 beziehen sich auf die jeweiligen MIPS-Zeilen. 
 
Da der Befehl in Zeile 3 nicht mehr auf das Schreiben in Register ```$t2``` warten muss, entfällt ein NOP. 
In Zeile 4 muss der ```add```-Befehl nicht auf das Schreiben in Register ```$t3``` warten und kann direkt in Register ```4($a0)``` gespeichert werden. Dadurch entfallen 2 weitere NOPs.
Analog ergibt sich somit der Unterschied in Zeile 6 und 7 zu Aufgabe 2.

###Aufgabe 4

Durch Umsortieren der Befehle ergibt sich das folgende Diagramm.

| Zeile | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
|   5   | LW        | ID     |  ``` 16($a0) ```| EX| ```$t4``` |  |  | | | | |
|   1   | |LW | ID | ``` 4($a0) ``` | EX             | ```$t1``` | | | | | |
|   2   |    |           | LW | ID             | ``` 8($a0) ``` | EX        | ```$t2``` | | | | |
|   6   |    |           |    | ADD     |  ID           | ```$t1,$t4```  | +  |```$t5``` | | | |
|   3   |    |           |    |         |ADD      | ID             | ```$t1,$t2``` | + | ```$t3``` | | |
|   7   |    |    |                |                || SW | ID |```$t5``` | EX | ```12($a0)``` | |
|   4   |    |    |                |                |           ||SW             | ID         |```$t3``` | EX | ``` 4($a0) ```|  


1-7 beziehen sich auf die jeweiligen MIPS-Zeilen. 
 
Durch forwarding kann der OS von Zeile 1 (B im Pseudocode) direkt an OF von Zeile 6 (B+E) gereicht werden. Der OS von diesem Befehl in Takt 8 kann dadurch an den OF von Zeile 7 (dem "D =") gereicht werden.
Analog dazu findet auch das forwarding in Takt 7 vom OS in Zeile 2 an OF in Zeile 3, sowie dem OS von Zeile 3 in Takt 9 an OF in Zeile 4 statt. 

Somit spart man sich NOPs, indem andere Befehle begonnen werden, während auf die Verfügbarkeit des Wortes durch einen Befehl gewartet wird. 

