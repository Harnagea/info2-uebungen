import Data.Char (toLower)

--Aufgabe 1.1
quadratic :: (Int, Int, Int) -> Int -> Int
quadratic (a,b,c) x = a*x*x + b*x + c

--Aufgabe 1.2
square :: Int -> Int
square n    | n < 0 = square(-n)
            | n == 0 = 0
            | otherwise = 2*n-1 + square(n-1)

--Aufgabe 1.3a)
sumList :: [Int] -> Int
sumList v   | length(v) == 0 = 0
            | length(v) == 1 = head v
            | otherwise = head v + sumList(tail v)

--Aufgabe 1.3b)
foldList :: (Double -> Double -> Double) -> [Double] -> Double
foldList f v    | length(v) == 1 = head v
                | otherwise = f (head v) (foldList f (tail v))

--Aufgabe 1.3c) 
mapList :: (Int -> Int) -> [Int] -> [Int]
mapList f [] = []
mapList f v = [f (head v)] ++ (mapList f (tail v))

--Aufgabe 1.4
tableInt :: (Int -> Int) -> [Int] -> String
tableInt f [a] = show (a) ++ ":" ++ show (f a)
tableInt f v = show (head v) ++ ":" ++ show (f (head v)) ++ "\n" ++ (tableInt f (tail v))

--Aufgabe 2.1
containsList :: [Int] -> Int -> Bool
containsList v x    | length(v) == 0 = False 
                    | head v == x = True
                    | otherwise = containsList (tail v) x

--Aufgabe 2.2
countList :: [Char] -> Char -> Int
countList [] x = 0
countList s x = if (toLower (head s) == toLower x) then 1 + countList (tail s) x else 0 + countList (tail s) x