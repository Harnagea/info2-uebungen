NUM=$(echo -n $1 | tr -d '\r\n')
tar cf "uebung0$NUM-testat.tar" "uebung0$NUM" 
until [ -f "./uebung0$NUM-testat.tar" ]
do 
	sleep 1
done
chmod a-w "uebung0$NUM-testat.tar" 

sha1sum "uebung0$NUM-testat.tar" 
