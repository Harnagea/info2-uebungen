.data
v:  .word 1, 2, 3, 4, 5, 6, 7
newline: .asciiz "\n"

.text
main:   la $a0, v       #Laden der ersten Adresse (zeigt auf 1)
        la $a1, 12($a0) #Laden der zweiten Adresse (zeigt auf 4)
        jal swap
        
        lw $t0, ($a0)   #Laden des Wertes von $a0, der ersten Adresse
        
        li $v0, 1
        move $a0, $t0
        syscall         #Ausgabe des Wertes der ersten Adresse (4)
        
        li $v0, 4
        la $a0, newline
        syscall

        li $v0, 1

        lw $t0, ($a1)   #Laden des Wertes von $a1, der zweiten Adresse
        move $a0, $t0
        syscall         #Ausgabe des Wertes der zweiten Adresse (1)
        
        li $v0, 10
        syscall         #Programmende
        

#Anscheinend kann man nicht sw ($a0), ($a1) machen
swap:   lw $t0, ($a0)
        lw $t1, ($a1)
        sw $t0, ($a1)
        sw $t1, ($a0)
        jr $ra