.data
v0: .word 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
v1: .word 1, 0, -1, -2, -3, -4, -5, -6, -7, -8
v2: .word 10, 2, 3, 4, 11, 5, 4, 3, 2, 12
space: .asciiz " "
newline: .asciiz "\n"

.text
main:
la $a0, v0
la $a1, 32($a0)
la $t5, v0
j sort

next_list:
li $v0, 4
la $a0, newline
syscall

addi $t5, $t5, 40   #auf den nächsten Listenanfang verschieben
move $a0, $t5
la $a1, 32($a0)
la $t6, v2          
bgt $t5, $t6, end_prog #Sprung zum Programmende, wenn t5 weiter als v2 zeigt

sort:
jal find

move $t2, $a1   #Endadresse zwischenspeichern

move $a1, $v0   #Adresse des höchsten Wertes

#lw $t4, ($a0)      
#bne $v1, $t4, swap  #wenn $v1 != $a0, dann swap durchführen, sonst lohnt es sich nicht da gleich
jal swap

move $a1, $t2  
addi $a0, $a0, 4    #Zeiger um 4 Stellen verschieben

beq $a0, $a1, finish #Wenn Endadresse == Startadresse, dann gibt es nichts mehr zum sortieren

j sort              #rekursiv aufrufen

finish:
move $t0, $t5             #Startadresse laden

output:                 #Logik zur Ausgabe des Feldes
li $v0, 1               #Int ausgeben
lw $a0, ($t0)
syscall

li $v0, 4
la $a0, space
syscall

bgt $t0, $a1, next_list  #Sobald $t0 > $a1, dann nächste Liste
addi $t0, $t0, 4        
j output                #Schleife

end_prog:
li $v0, 10
syscall

############################
#find-Algorithmus
############################

find:
move $v0, $a0       #Startwerte laden
lw $v1, ($a0)
move $t1, $a0

while:
bgt $t1, $a1, end   #Endbedingung, dass $a0 eine höhere Adresse als $a1 ist, also ende erreicht

addi $t1, $t1, 4    #$a0 um 4 Byte verschieben
lw $t0, ($t1)       #Wert laden
bgt $t0, $v1, override  #Wenn neuer Wert größer als alter, dann Sprung zu override

j while             #Sprung zu while

override:
lw $v1, ($t1)       #Überschreiben von $v1 mit Wert von ($a0)
move $v0, $t1       #Überschreiben der Adresse

j while             #Sprung zu while

end:
jr $ra              #Ende des Algorithmus

##########################
#swap-Algorithmus
##########################

swap:   lw $t0, ($a0)
        lw $t1, ($a1)
        sw $t0, ($a1)
        sw $t1, ($a0)
        jr $ra