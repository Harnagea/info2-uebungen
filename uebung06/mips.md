#Informatik II, Übung 06, Aufgabe 2

Markdown und AsciiMath

### 1 Load Half

*Unsigned*

Registerinhalt: 0x0000A80D

Beim Laden eines unsigned values findet keine Biterweiterung der höchsten Stelle statt, d.h. der Wert wird unverändert ins Register geladen.

*Signed*

Registerinhalt: 0xFFFFA80D

Hier findet eine Biterweiterung der höchsten Stelle statt. Da $0xA = (1010)_(2)$ ist, wird das höchste Bit (1) auf die darauffolgenden Bits übertragen, damit die Darstellung im Two's Complement korrekt bleibt.

### 2 Assembler Lesen

*a)*

```
.data
sequence : .word 2 , -5 , 3 , -90 , 300 , -54 , -23 , -35 , 120 , -54   #Belegen eines Feldes mit Werten
len: .word 10   #Belegen eines Wortes mit der Länge des Feldes

.text
main:   la $t0 , sequence   #Laden der Anfangsadresse vom Feld sequence in $t0
        lw $t1 , len    #Laden des Wertes von len nach $t1
        li $t2 , 0      #Belegen von $t2 mit dem Wert 0

l:      lw $s0 , ( $t0) #Wert, auf den $t0 zeigt, nach $s0 laden
        bgez $s0 , p    #Wenn der Wert von $s0 >= 0, dann Sprung nach p
        addi $t2 , $t2 , 1  #den Wert von $t2 um 1 erhöhen. Wird nur erreicht, wenn $s0 < 0, also negativ 

p:      add $t0 , $t0 , 4   #den Wert von $t0 um 4 erhöhen (zeigt auf das nächste Wort im Feld sequence)
        sub $t1 , $t1 , 1   #von $t1 1 abziehen
        bgtz $t1 , l        #wenn $1 > 1, dann Sprung nach l
```

*b)*

Der Programmanfang zählt alle negativen Werte in einem Feld mit gegebener Länge.

In ```$t2``` befindet sich nach Ablauf der Wert 7, da 7 negative Zahlen in der ```sequence``` definiert sind

### Assembler und Rekursion

*a)* 

```
#Bilden des Rekursionsbaum
fac:      addi    $sp, $sp, -12 #verschieben des stack pointers um 12 Speicherstellen nach vorne
          sw      $ra, 12($sp)  #speichern der return address an der 12. Speicherstelle
          sw      $fp, 8($sp)   #zwischenspeichern des frame pointers an der 8. Speicherstelle
          add     $fp, $sp, 12  #überschreiben des frame pointers, damit dieser auf die return address zeigt
          sw      $a0, 4($sp)   #speichern von n an der 4. speicherstelle

          li      $v0, 1        #1 in v0 schieben, da es das neutrale element für die multiplikation ist
          sub     $a0, $a0, 1   #n = n - 1

          blez    $a0,  return  #wenn n < 0, sprung zu return

rec:	  jal     fac           #sprung nach fac, return address zeigt auf nachfolgende zeile 

#Berechnen der Fakultät
          lw      $a0, 4($sp)   #laden vom untersten n im stack
          mult    $a0, $v0      #multiplikation von n mit v0
          mflo    $v0           #Produkt der Fakultät

#Auflösen des Rekursionsbaumes
return:
          lw      $fp, 8($sp)  #laden des vorigen frame pointers aus dem stack
          lw      $ra, 12($sp) #laden der vorigen return address aus dem stack
          add     $sp, $sp, 12 #verschieben des stack pointers um 12 stellen zurück

          jr      $ra          #sprung zur return address, wonach die fakultät berechnet wird
```

*b)*

```
| Stacksegment                  |
| ($ra) 0x0040 0022             | 0x1000 FFF0
| ($fp) 0x1001 0001             | 
| ($a0) 0x0000 0003             |
| ($ra) 0x0040 0022             | 0x1000 FFE5
| ($fp) 0x1000 FFF0             |
| ($a0) 0x0000 0002             |
| ($ra) 0x0040 0022             | 0x1000 FFDA
| ($fp) 0x1000 FFE5             |
| ($a0) 0x0000 0001             |
| ($ra) 0x0040 0022             |
| ($fp) 0x1000 FFDA             |
| ($a0) 0x0000 0000             |
+ -- - - - - - - - - - - - -    +
| Datensegment                  |
|                               |
+ -- - - - - - - - - - - - -    + 0x1000 0000
| Textsegment                   |
| fac                           | 0x0040 0080
| rec                           | 0x0040 005C
| return                        | 0x0040 0038
+ -- - - - - - - - - - - - -    + 0x0040 0000
| reserviert                    |
+ -- - - - - - - - - - - - -    + 0x0000 0000

```