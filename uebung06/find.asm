.data
v0: .word 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
v1: .word 1, 0, -1, -2, -3, -4, -5, -6, -7, -8
v2: .word 10, 2, 3, 4, 11, 5, 4, 3, 2, 12

.text
main:
la $a0, v2
la $a1, 32($a0)     #Offset von 32 da 4 Byte * 8 Speicherzellen, also $a1 auf die letzte Zelle zeigt

jal find            #Beginn des Such-Algorithmus

li $v0, 1
move $a0, $v1       
syscall             #Ausgabe des Elementes

li $v0, 10
syscall             #Programmende

find:
move $v0, $a0       #Startwerte laden
lw $v1, ($a0)
move $t1, $a0
while:
bgt $t1, $a1, end   #Endbedingung, dass $a0 eine höhere Adresse als $a1 ist, also ende erreicht

addi $t1, $t1, 4    #$a0 um 4 Byte verschieben
lw $t0, ($t1)       #Wert laden
bgt $t0, $v1, override  #Wenn neuer Wert größer als alter, dann Sprung zu override

j while             #Sprung zu while

override:
lw $v1, ($t1)       #Überschreiben von $v1 mit Wert von ($a0)
move $v0, $t1       #Überschreiben der Adresse

j while             #Sprung zu while

end:
jr $ra              #Ende des Algorithmus