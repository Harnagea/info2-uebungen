.data
v:		.word 1, 2, 3, 4, 5, -3, 55, 13, -222, 10, 400
s:		.asciiz " "

.text
main:
la		$a0		v
la		$a1		40($a0)
move	$t6		$a0
move	$t7		$a1
jal		sorting
stop:
while2:
bgt		$t6		$t7		end2
lw		$a0		0($t6)
li		$v0		1
syscall
la		$a0		s
li		$v0		4
syscall
addi	$t6		4
j		while2
end2:
li		$v0		10
syscall

sorting:
bge		$a0		$a1		stop
move	$t3		$a0
move	$t4		$a1
jal		find
move	$a0		$t3
move	$a1		$t4
move	$t5		$a1
move	$a1		$v0
jal		swap
move	$a1		$t5
addi	$a0		4
jal		sorting
# stop:
# # jr		$ra
# li		$v0		10
# syscall

swap:
lw		$t0		0($a0)
lw		$t1		0($a1)
sw		$t0		0($a1)
sw		$t1		0($a0)
jr		$ra

find:
lw		$v1		0($a0)
move	$v0		$a0
while:
bgt		$a0		$a1		end
lw		$t0		0($a0)
bgt		$t0		$v1		greater
j		increment
greater:
move 	$v1		$t0
move	$v0		$a0
increment:
addi	$a0		4
j		while
end:
jr		$ra