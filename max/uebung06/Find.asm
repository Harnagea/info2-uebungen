.data
v:		.word 1, 2, 3, 4, 5

.text
main:
la		$a0		v
la		$a1		16($a0)
jal		find
lw		$a0		0($v0)
li		$v0		1
syscall
li		$v0		10
syscall

find:
lw		$v1		0($a0)
move	$v0		$a0
while:
bgt		$a0		$a1		end
lw		$t0		0($a0)
bgt		$t0		$v1		greater
j		increment

greater:
move 	$v1		$t0
move	$v0		$a0

increment:
addi	$a0		4

j		while
end:
jr		$ra