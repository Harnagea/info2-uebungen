.data
v:		.word 1, 2, 3, 4, 5

.text
main:
la		$a0		v
la		$a1		12($a0)
jal		swap
li		$v0		10
syscall

swap:
lw		$t0		0($a0)
lw		$t1		0($a1)
sw		$t0		0($a1)
sw		$t1		0($a0)
jr		$ra