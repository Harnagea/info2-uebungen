.data

.text
main:
li		$v0		5
syscall
move	$t0		$v0
bgez	$t0		pos
move	$t1		$t0
add 	$t1		$t1		$t1
sub		$t0		$t0		$t1
pos:
move	$t1		$t0					#$t1 = |n|
li		$t0		0					#$t0 ist letztendlich Ergebnis
li		$t2		1					#$t2 ist Laufvariable
while:
add		$t3		$t2		$t2
addi	$t3		-1
add		$t0		$t0		$t3
addi	$t2		1
sub		$t4		$t1		$t2
bgez	$t4		while
move	$a0		$t0
li		$v0		1
syscall
li		$v0		10
j		main