data Prozess = Prozess {pid :: String, arrival :: Int, computing :: Int} deriving (Show)

instance Eq Prozess where
 x == y = computing x == computing y
 
instance Ord Prozess where
 compare x y
  |computing x < computing y = LT
  |computing x > computing y = GT
  |otherwise = EQ
  
data State = State {new :: [Prozess], run :: Prozess, ready :: [Prozess], time :: Int, chart :: String}

idle = Prozess{pid = "IDLE", arrival = -1, computing = -1}
ps = [Prozess{pid = "P1", arrival = 0 , computing = 6}]
lectureExample = State{new = ps, run = idle, ready = [], time = 0,chart ="" }


instance Show State where
 show s = "--new\n"++showProcessList(new s)++"--run\n"++show(run s)++"--ready\n"++showProcessList(ready s)++"--time: "++show(time s)++"\n--chart: "++chart s
 
showProcessList :: [Prozess] -> String
showProcessList [] = ""
showProcessList l = show(head l) ++ "\n" ++ showProcessList(tail l)