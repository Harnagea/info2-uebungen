.data
v:		.double 1.0
n:		.double 5.0

.text
main:
li		$v0		7
syscall
# mov.d	$f12	$f0
jal		fac
li		$v0		3
syscall
li		$v0		10
syscall

fac:
mfc1	$t0		$f0
mfc1	$t1		$f1
li.d	$f2		0.0
c.lt.d	$f0		$f2
bc1t	negative
li.d	$f2		1.0				#konstante 1
li.d	$f12	1.0				#Rückgabewert
while:
c.lt.d	$f0		$f2
bc1t	exit
mul.d	$f12	$f12	$f0
sub.d	$f0		$f0		$f2
j		while
exit:
mtc1	$t0		$f0
mtc1	$t1		$f1
jr		$ra
negative:
li.d	$f12	0.0
j		exit
