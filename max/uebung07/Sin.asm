.text

main:						#x in (f0, f1), i in (f2, f3)

li.d	$f0		5.0
li.d	$f2		3.0

mfc1	$t0		$f0
mfc1	$t1		$f1
mfc1	$t2		$f2
mfc1	$t3		$f3
mfc1	$t4		$f4
mfc1	$t5		$f5

li.d	$f4		1.0
add.d	$f2		$f2		$f2		#i verdoppeln
sub.d	$f2		$f2		$f4		#i--
mfc1	$t6		$f2
mfc1	$t7		$f3
while2:
c.le.d	$f2		$f4
bc1t	exit2
mul.d	$f0		$f0		$f0
sub.d	$f2		$f2		$f4
j		while2

exit2:

mov.d	$f12	$f0
li		$v0		3
syscall
li		$v0		10
syscall

mtc1	$t0		$f0
mtc1	$t1		$f1
mtc1	$t2		$f2
mtc1	$t3		$f3
mtc1	$t4		$f4
mtc1	$t5		$f5


# fac:
# mfc1	$t0		$f0
# mfc1	$t1		$f1
# li.d	$f2		0.0
# c.lt.d	$f0		$f2
# bc1t	negative
# li.d	$f2		1.0				#konstante 1
# li.d	$f12	1.0				#Rückgabewert
# while:
# c.lt.d	$f0		$f2
# bc1t	exit
# mul.d	$f12	$f12	$f0
# sub.d	$f0		$f0		$f2
# j		while
# exit:
# mtc1	$t0		$f0
# mtc1	$t1		$f1
# jr		$ra
# negative:
# li.d	$f12	0.0
# j		exit
