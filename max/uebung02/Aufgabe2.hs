type Nibble = (Bool, Bool, Bool, Bool)

showNibble :: Nibble -> String
showNibble n = toBinary n ++ "  " ++ show(toDecimal n) ++ "  " ++ show(to2complement n)

toBinary :: Nibble -> String
toBinary (a, b, c, d) = show(if a then 1 else 0) ++ show(if b == True then 1 else 0) ++ show(if c == True then 1 else 0) ++ show(if d == True then 1 else 0)

toDecimal :: Nibble -> Int
toDecimal (a, b, c, d) = (if a then 8 else 0) + (if b then 4 else 0) + (if c then 2 else 0) + (if d then 1 else 0)

to2complement :: Nibble -> Int
to2complement (a, b, c, d) = if toDecimal (a, b, c, d) <= 7 then toDecimal (a, b, c, d) else toDecimal (a, b, c, d) - 16 

fulladder :: Bool -> Bool -> Bool -> (Bool, Bool)
fulladder a b c = ((a&&b)||(a&&c)||(b&&c),((not(a&&b)&&not(a&&c)&&not(b&&c))&&(a||b||c))||(a&&b&&c))

rippleCarryAdder :: Nibble -> Nibble -> Nibble
rippleCarryAdder n m = (x > 7, mod x 8 > 3, mod x 4 > 1, mod x 2 > 0)
 where x = toDecimal n + toDecimal m
 
tableAdder :: (Nibble -> Nibble -> Nibble) -> [(Nibble, Nibble)] -> String
tableAdder f [] = ""
tableAdder f l = showNibble (fst (head l)) ++ " ; " ++ showNibble (snd (head l)) ++ " = " ++ showNibble (f (fst (head l)) (snd (head l))) ++ "\n" ++ tableAdder f (tail l)