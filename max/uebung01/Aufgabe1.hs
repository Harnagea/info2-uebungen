import Data.Char (toLower)

--Input: quadratic(1, 1, 0) 5
quadratic :: (Int, Int, Int) -> (Int -> Int)
quadratic (a, b, c) x = a*x*x + b*x + c

--Input: square 12
square :: Int -> Int
square n
 |n < 0 = square (-n)
 |n == 0 = 0
 |otherwise = 2*n-1 + square (n-1)

--Input: sumList [1..10]
sumList :: [Int] -> Int
sumList [] = 0
sumList a = head a + sumList (tail a)

--Input: foldList (*) [1..10]
foldList :: (Double -> Double -> Double) -> [Double] -> Double
foldList f [a] = a
foldList f l = f (head l) (foldList f (tail l))

--Input: mapList square [1..10]
mapList :: (Int -> Int) -> [Int] -> [Int]
mapList f [] = []
mapList f l = concat [[f (head l)], mapList f (tail l)]

--Input: putStrLn (tableInt square [1..10])
tableInt :: (Int -> Int) -> [Int] -> String
tableInt f [a] = show(a) ++ ":" ++ show((f (a)))
tableInt f l = show (head l) ++ ":" ++ show ((f (head l))) ++ "\n" ++ tableInt f (tail l)

--Input: containsList [1..10] 3
containsList :: [Int] -> Int -> Bool
containsList [] n = False
containsList l n = head l == n || containsList (tail l) n

--Input: "Hier könnte ihre Werbung stehen" 'h'
countList :: [Char] -> Char -> Int
countList [] c = 0
countList s c = if(toLower (head s) == toLower c) then 1 + countList (tail s) c else 0 + countList (tail s) c