type Nibble = (Bool, Bool, Bool, Bool)
type Byte = (Nibble, Nibble)

intToByte2complement :: Int -> Byte
intToByte2complement i = if (i >= 0 && i < 128) then intToByte i else rippleCarryAdder (intToByte 1) (invertByte (intToByte (-i)))

intToByte :: Int -> Byte
intToByte i = ((i >= 128, mod i 128 >= 64, mod i 64 >= 32, mod i 32 >= 16), (mod i 16 >= 8, mod i 8 >= 4, mod i 4 >= 2, mod i 2 >= 1))

fulladder :: Bool -> Bool -> Bool -> (Bool, Bool)
fulladder a b c = ((a&&b)||(a&&c)||(b&&c),((not(a&&b)&&not(a&&c)&&not(b&&c))&&(a||b||c))||(a&&b&&c))

invertByte :: Byte -> Byte
invertByte ((a, b, c, d), (e, f, g, h)) = ((not a, not  b, not c, not d), (not e, not f, not g, not h))

showNibble :: Nibble -> String
showNibble (a, b, c, d) = show(if a then 1 else 0) ++ show(if b == True then 1 else 0) ++ show(if c == True then 1 else 0) ++ show(if d == True then 1 else 0)

showByte :: Byte -> String
showByte ((a, b, c, d), (e, f, g, h)) = show(if a then 1 else 0) ++ show(if b == True then 1 else 0) ++ show(if c == True then 1 else 0) ++ show(if d == True then 1 else 0) ++ show(if e then 1 else 0) ++ show(if f == True then 1 else 0) ++ show(if g == True then 1 else 0) ++ show(if h == True then 1 else 0)

shiftByte :: Byte -> Byte
shiftByte ((a, b, c, d), (e, f, g, h)) = ((b, c, d, e), (f, g, h, False))

signExtension :: Nibble -> Byte
signExtension (a, b, c, d) = ((a, a, a, a), (a, b, c, d))

multBitByte :: Bool -> Byte -> Byte
multBitByte False b = ((False, False, False, False), (False, False, False, False))
multBitByte True b = b

rippleCarryAdder :: Byte -> Byte -> Byte
rippleCarryAdder ((a1, b1, c1, d1), (e1, f1, g1, h1)) ((a2, b2, c2, d2), (e2, f2, g2, h2)) = ((a3, b3, c3, d3), (e3, f3, g3, h3))
 where
  (carry1, h3) = fulladder h1 h2 False
  (carry2, g3) = fulladder g1 g2 carry1
  (carry3, f3) = fulladder f1 f2 carry2
  (carry4, e3) = fulladder e1 e2 carry3
  (carry5, d3) = fulladder d1 d2 carry4
  (carry6, c3) = fulladder c1 c2 carry5
  (carry7, b3) = fulladder b1 b2 carry6
  (carry8, a3) = fulladder a1 a2 carry7
  
carrySaveAdder :: Byte -> Byte -> Byte -> (Byte, Byte)
carrySaveAdder ((a1, b1, c1, d1), (e1, f1, g1, h1)) ((a2, b2, c2, d2), (e2, f2, g2, h2)) ((a3, b3, c3, d3), (e3, f3, g3, h3)) = (((a4, b4, c4, d4), (e4, f4, g4, h4)), ((a5, b5, c5, d5), (e5, f5, g5, h5)))
 where
  (a4, a5) = fulladder a1 a2 a3
  (b4, b5) = fulladder b1 b2 b3
  (c4, c5) = fulladder c1 c2 c3
  (d4, d5) = fulladder d1 d2 d3
  (e4, e5) = fulladder e1 e2 e3
  (f4, f5) = fulladder f1 f2 f3
  (g4, g5) = fulladder g1 g2 g3
  (h4, h5) = fulladder h1 h2 h3
  
negByte :: Byte -> Byte
negByte b = rippleCarryAdder (intToByte 1) (invertByte b)
  
multNibble :: Nibble -> Nibble -> Byte
multNibble n1 n2 = rippleCarryAdder (multBitByte h n1Ext) (rippleCarryAdder (shiftByte (multBitByte g n1Ext)) (rippleCarryAdder (shiftByte (shiftByte (multBitByte f n1Ext))) (rippleCarryAdder (shiftByte (shiftByte (shiftByte (multBitByte e n1Ext)))) (rippleCarryAdder (shiftByte (shiftByte (shiftByte (shiftByte (multBitByte d n1Ext))))) (rippleCarryAdder (shiftByte (shiftByte (shiftByte (shiftByte (shiftByte (multBitByte c n1Ext)))))) (rippleCarryAdder (shiftByte (shiftByte (shiftByte (shiftByte (shiftByte (shiftByte (multBitByte b n1Ext))))))) (shiftByte (shiftByte (shiftByte (shiftByte (shiftByte (shiftByte (shiftByte (multBitByte a n1Ext))))))))))))))
 where
  n1Ext = signExtension n1
  ((a, b, c, d), (e, f, g, h)) = signExtension n2

-- tableMult multNibble [((False, True, False, True),(False, False, True, True))]
tableMult :: (Nibble -> Nibble -> Byte) -> [(Nibble, Nibble)] -> String
tableMult f [] = ""
tableMult f l = showNibble (fst (head l)) ++ " * " ++ showNibble (snd (head l)) ++ " = " ++ showByte (f (fst (head l)) (snd (head l))) ++ "\n" ++ tableMult f (tail l)
