import Data.List

data Prozess = Prozess { pid :: String, arrival :: Int, computing :: Int} deriving(Show)

instance Eq Prozess where
 x == y = computing x == computing y
 
instance Ord Prozess where
 compare x y
  |computing x < computing y = LT
  |computing x > computing y = GT
  |otherwise = EQ
  
data State = State {new :: [Prozess], run :: Prozess, ready :: [Prozess], time :: Int, chart :: String}

instance Show State where
 show s = "--new\n" ++ showProcessList(new s) ++ "--run\n" ++ show(run s) ++ "\n--ready\n" ++ showProcessList(ready s) ++ "--time:" ++ show(time s) ++ "\nchart:" ++ show(chart s)

update_ready :: State -> State
-- update_ready s = State{ new = fst(update_helper(new s, ready s) (time s)),
						-- run = run s,
						-- ready = snd(update_helper(new s, ready s) (time s)),
						-- time = time s,
						-- chart = chart s}
						
-- update_ready s = State{ new = fst(update_helper(new s, ready s) (time s)),
						-- run = run s,
						-- ready = sort(snd(update_helper(new s, ready s) (time s))),
						-- time = time s,
						-- chart = chart s}
update_ready s = if time s == arrival (head (new s)) then 
					State{ new = tail(fst(update_helper(new s, ready s))
							run = head(fst(update_helper(new s, ready s))
							ready = 
 
update_helper :: ([Prozess], [Prozess]) -> Int -> ([Prozess], [Prozess])
update_helper ([], []) i = ([], [])
update_helper ([], r) i = ([], r)
update_helper (n, r) i = if arrival(head n) <= i then update_helper(tail n, r ++ [head n]) i else (n, r)
 
update_run :: State -> State
update_run s = if arrival (run s) == -1 then State{new = new s, run = head (ready s), ready = tail (ready s), time = time s, chart = chart s} else s
 
update_time :: State -> State
update_time s =State{new = new s, 
					run = if computing (run s) == 1 then idle else Prozess{pid = pid (run s), arrival = arrival (run s), computing = (computing (run s))-1},
					ready = ready s,
					time = (time s)+1,
					chart = chart s ++ pid (run s)}

update :: State -> Int -> State
update s 0 = s
update s i = update_time(update_run(update_ready (update s (i-1))))
					
showProcessList :: [Prozess] -> String
showProcessList [] = ""
showProcessList (x:xs) = show x ++ "\n" ++ showProcessList xs
 
 
idle = Prozess{pid = "IDLE", arrival = -1, computing = -1}
ps =   [Prozess{pid = "p1", arrival = 0, computing = 6},
		Prozess{pid = "p2", arrival = 2, computing = 6},
		Prozess{pid = "p3", arrival = 4, computing = 5},
		Prozess{pid = "p4", arrival = 12, computing = 4},
		Prozess{pid = "p5", arrival = 16, computing = 3},
		Prozess{pid = "p6", arrival = 19, computing = 6}]
lectureExample = State{new = ps, 
					   run = idle, 
					   ready = [], 
					   time = 1, 
					   chart = ""}
